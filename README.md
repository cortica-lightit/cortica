# [cortica-scrapper] #

## Short Description ##

This project is created to parsing users and user's photo.

## Used technologies ##
 
`python3`, `scrapy`, `mongodb`, `boto3`

## Project setup ##

**Before cloning** the project - make sure that you have installed all of the packages.

`sudo apt-get install -y build-essential wget g++ git python3.5 python3.5-dev libxml2-dev python3-pip libffi-dev mongodb`

* You can install mongodb from docker images instead of deb or you can just provide another connect credentials in settings module. 

And clone the repo.

**After cloning** the repo you must setup environment in project dir.

Enter in you bash shell: `virtualenv .env -p /usr/bin/python3.5`.

Activate the environment `source .env/bin/activate`.

You should update the pip and the setuptools: `pip3 install pip setuptools -U`.

Next you must install requirement libraries: `pip3 install -r requirements.txt`.

### Setup application ###

Create `settings_local.py` file and move to `{project_directory}/cortica/` directory (optional).

Example of settings file.

```
# This is credentials for the BHARATSTUDENT spider to parse users.
BHARATSTUDENT_USERNAME = 'email'
BHARATSTUDENT_PASSWORD = 'password'


# If you set None to both params - amazon will be check key in ~/.aws/credentials
AWS_ACCESS_KEY_ID = 'amazon_secret_key_id'
AWS_SECRET_ACCESS_KEY = 'amazon_secret_access_key'

# if None - will be ignored an use another credentials
MONGODB_URI = None


# SPIDERS_DATA_DIR - You can provide spiders cache dir mannualy

# USERS_TABLE_NAME - Table name for the User model, `user` by default
```

## Spiders ##

Each spider takes base arguments:

* `min_photo_count` - Minimum photo quantity a user has. If photo quantity is less than provided number, the spider will ignore that user. Default: `10`. 

### bharatstudent.com ###

For that spider you must provide the credentials to account on that website in settings module.
