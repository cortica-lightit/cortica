# -*- coding: utf-8 -*-
import os
import shutil
import boto3
from scrapy.exceptions import DropItem
from urllib.request import urlretrieve
from botocore.exceptions import ClientError

from cortica.items import (
    UserItem,
    PhotoItem
)
from cortica.models import UserModel
from cortica.settings import (
    AWS_S3_FOLDER_MASK_NAME,
    AWS_S3_REGION,
    AWS_S3_BUCKET_NAME,
    AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY
)


class PipelineException(Exception):
    pass


class UtilsPipeline(object):

    @staticmethod
    def get_spider_item_data_dir(item, spider):
        return '{}/{}'.format(
            os.path.abspath(spider.spider_data_dir),
            item['profile_unique_id']
        )

    @staticmethod
    def generate_s3_folder(item):
        return AWS_S3_FOLDER_MASK_NAME.format(**item)

    def generate_s3_url(self, item):
        return 'https://{bucket}.s3.{region}.amazonaws.com/{folder}'.format(
            bucket=AWS_S3_BUCKET_NAME,
            region=AWS_S3_REGION,
            folder=self.generate_s3_folder(item)
        )


class UserPipeline(UtilsPipeline):

    def process_item(self, item, spider):
        if not isinstance(item, UserItem):
            return item

        item_dict = dict(item)
        item_dict['folder_s3_url'] = self.generate_s3_url(item)

        try:
            UserModel.objects.get({
                'profile_unique_id': item_dict['profile_unique_id'],
                'spider_name': item_dict['spider_name']
            })
        except UserModel.DoesNotExist:
            user = UserModel(**item_dict)
            user.save()
        else:
            raise DropItem(
                'User with {!r} unique id for {!r} '
                'spider already exist.'.format(
                    item_dict['profile_unique_id'],
                    item_dict['spider_name']
                )
            )

        return item


class PhotoLocalSavePipeline(UtilsPipeline):

    def process_item(self, item, spider):
        if not isinstance(item, PhotoItem):
            return item

        item_dir = self.get_spider_item_data_dir(item, spider)

        spider.create_spider_data_dir()
        if not os.path.exists(item_dir):
            os.mkdir(item_dir)

        try:
            # Download image
            retrieved = urlretrieve(item['img_url'])
        except Exception as e:
            msg = 'Image from {!r} spider was not uploaded. {}'.format(
                spider.name, e)
            spider.increment_stat('image_not_uploaded')
            raise DropItem(msg)

        item['img_name'] = item['img_url'].split('/')[-1]

        item['img_local_path'] = '{}/{}'.format(
            item_dir, item['img_name'])
        shutil.move(retrieved[0], item['img_local_path'])

        return item


class PhotoS3SavePipeline(UtilsPipeline):

    __s3 = None

    @classmethod
    def _create_bucket_if_not_exist(cls):
        s3 = cls.get_s3()
        try:
            s3.meta.client.head_bucket(Bucket=AWS_S3_BUCKET_NAME)
        except ClientError as e:
            # http://boto3.readthedocs.io/en/latest/guide/migrations3.html#accessing-a-bucket  # noqa
            error_code = int(e.response['Error']['Code'])
            if error_code == 404:
                # Create bucket
                s3.create_bucket(Bucket=AWS_S3_BUCKET_NAME)
                # Set public permission for the bucket
                s3.Bucket(AWS_S3_BUCKET_NAME).Acl().put(ACL='public-read')
            else:
                raise PipelineException('Wrong AWS credentials.') from e

    @classmethod
    def get_s3(cls):
        if not cls.__s3:
            cls.__s3 = boto3.resource(
                's3',
                aws_access_key_id=AWS_ACCESS_KEY_ID,
                aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                region_name=AWS_S3_REGION)
            cls._create_bucket_if_not_exist()
        return cls.__s3

    @property
    def s3(self):
        return self.get_s3()

    def process_item(self, item, spider):
        if not isinstance(item, PhotoItem):
            return item

        img = self.s3.Object(
            AWS_S3_BUCKET_NAME,
            '{}/{}'.format(
                self.generate_s3_folder(item),
                item['img_name']
            )
        )

        img.upload_file(item['img_local_path'])
        # Make public
        img.Acl().put(ACL='public-read')

        return item


class PhotoLocalRemovePipeline(UtilsPipeline):

    def process_item(self, item, spider):
        if not isinstance(item, PhotoItem):
            return item

        if os.path.exists(item['img_local_path']):
            os.remove(item['img_local_path'])

        item_dir = self.get_spider_item_data_dir(item, spider)
        if not os.listdir(item_dir):
            try:
                os.rmdir(item_dir)
            except OSError:
                # Not empty dir
                pass
            except FileNotFoundError:
                # Path already removed
                pass

        return item
