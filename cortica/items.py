# -*- coding: utf-8 -*-
import scrapy


class BaseItem(scrapy.Item):

    profile_unique_id = scrapy.Field()
    spider_name = scrapy.Field()


class UserItem(BaseItem):

    user_name = scrapy.Field()
    user_dob = scrapy.Field()
    user_gender = scrapy.Field()
    user_location = scrapy.Field()
    folder_s3_url = scrapy.Field()
    photos_count = scrapy.Field()


class PhotoItem(BaseItem):

    img_url = scrapy.Field()
    img_local_path = scrapy.Field()
    img_name = scrapy.Field()
