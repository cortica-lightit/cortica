import os
import shutil

from scrapy import Spider

from cortica.models import UserModel
from cortica.items import (
    UserItem,
    PhotoItem
)
from cortica.settings import SKIP_EXISTING_USERS, SPIDERS_DATA_DIR


class BaseSpider(Spider):

    min_photo_count = None

    def __init__(self, min_photo_count=10, *args, **kwargs):
        super(BaseSpider, self).__init__(*args, **kwargs)
        self.min_photo_count = int(min_photo_count)

    @staticmethod
    def extract(xpath, is_strip=True):
        tmp = xpath.extract_first()
        return (tmp.strip() if is_strip else tmp) if tmp else None

    @staticmethod
    def get_user_item(**kwargs):
        return UserItem(**kwargs)

    @staticmethod
    def get_photo_item(**kwargs):
        return PhotoItem(**kwargs)

    @staticmethod
    def get_user_model():
        return UserModel

    def check_photo_count(self, count):
        if not self.min_photo_count:
            return True
        return count >= self.min_photo_count

    def set_item_default_field(self, response, item):
        item['spider_name'] = self.name
        return item

    def check_user_for_exist(self, profile_unique_id):
        if not SKIP_EXISTING_USERS:
            return True

        model = self.get_user_model()
        try:
            model.objects.get({
                'profile_unique_id': profile_unique_id,
                'spider_name': self.name
            })
        except model.DoesNotExist:
            return True

        return False

    def increment_stat(self, value):
        self.crawler.stats.inc_value('spider/{}/{}'.format(self.name, value))

    @property
    def spider_data_dir(self):
        return os.path.join(SPIDERS_DATA_DIR, self.name)

    def create_spider_data_dir(self):
        if not os.path.exists(self.spider_data_dir):
            os.mkdir(self.spider_data_dir)

    def remove_spider_data_dir(self):
        if os.path.exists(self.spider_data_dir):
            shutil.rmtree(self.spider_data_dir)
