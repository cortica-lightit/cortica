# -*- coding: utf-8 -*-
from logging import INFO
from scrapy import Request, FormRequest

from cortica.base_spider import BaseSpider
from cortica.settings import (
    BHARATSTUDENT_USERNAME as USERNAME,
    BHARATSTUDENT_PASSWORD as PASSWORD
)


class BharatstudentcomSpider(BaseSpider):

    user_list = set()

    name = 'bharatstudent.com'
    allowed_domains = ['bharatstudent.com']

    BASE_URL = 'http://www.bharatstudent.com'
    LOGIN_URL = '{}/fbmylogin.php'
    PHOTO_URL = '{}/photos.php'

    @property
    def login_url(self):
        return self.LOGIN_URL.format(self.BASE_URL)

    @property
    def photo_url(self):
        return self.PHOTO_URL.format(self.BASE_URL)

    def start_requests(self):
        if not USERNAME or \
                not PASSWORD:
            raise AttributeError('Username or Password is absent.')

        # Login
        yield FormRequest(
            url=self.login_url,
            method='POST',
            headers={},
            formdata={
                'email': USERNAME,
                'password': PASSWORD,
                'button': 'Login',
                'keywordv': 'loginok'
            },
            meta={
                'dont_redirect': True,
                'handle_httpstatus_all': True
            },
            callback=self.login,
        )

    def login(self, response):
        if response.status != 302:
            raise AttributeError('Username or Password is wrong.')

        # Photo posts
        yield Request(
            self.photo_url,
            self.parse
        )

    def parse(self, response):
        for post in response.xpath('//p/a[@class="greylink"]/@href'):
            yield Request(
                '{}/{}'.format(self.BASE_URL, post.extract()),
                self.parse_preview
            )

        # Next photo page
        next_page = self.extract(response.xpath('//a[text()="Next"]/@href'))
        if not next_page:
            return

        yield Request(
            '{}/{}'.format(self.BASE_URL, next_page),
            self.parse
        )

    def parse_preview(self, response):
        user_profile_link = self.extract(response.xpath(
            '//td[contains(text(), "Uploaded")]/a/@href'))
        return Request(
            '{}/{}'.format(self.BASE_URL, user_profile_link),
            self.parse_user_info
        )

    def parse_user_info(self, response):
        if response.xpath('//td[contains(text(), "no access")]'):
            self.increment_stat('closed_profile')
            self.log('No access: {!r}'.format(response.url), INFO)
            return

        try:
            photo_count = \
                int(response.xpath('//td[contains(text(), "Photos")]')
                    .re(r'\((\d+)\)')[0])
        except (AttributeError, ValueError, LookupError):
            if response.url.endswith('index.php'):
                self.increment_stat('wrong_user_info_link')
                self.log('Wrong user link.')
                return

            raise AttributeError('Photo count was not found.')

        if not self.check_photo_count(photo_count):
            self.increment_stat('skipped_profile_few_photo')
            self.log('Skip profile - too few photos.')
            return

        photo_album_link = self.extract(response.xpath(
            '//tr[td[contains(text(), "Photos")]]/td[3]/a/@href'))
        if not photo_album_link:
            raise AttributeError('Photo album link was not found.')

        full_profile_link = self.extract(response.xpath(
            '//a[contains(text(), "ull profile")]/@href'))
        if not photo_album_link:
            raise AttributeError('Full profile link was not found.')

        yield Request(
            '{}/{}'.format(self.BASE_URL, full_profile_link),
            self.parse_full_profile,
            meta={
                'photo_count': photo_count,
                'photo_album_link': photo_album_link
            }
        )

    def _check_duplicate_user(self, nickname):
        if nickname in self.user_list:
            return True

        self.user_list.add(nickname)

        return False

    @staticmethod
    def _parse_td2(response, contains):
        return response.xpath(
            '//tr[td[contains(text(), "{}")]]/td[2]'.format(contains))

    def parse_full_profile(self, response):
        user = self.set_item_default_field(response, self.get_user_item())

        def xp(contains, xpath):
            return self.extract(
                self._parse_td2(response, contains).xpath(xpath)
            )

        user['profile_unique_id'] = xp('Nick Name', 'text()')
        if self._check_duplicate_user(user['profile_unique_id']) or \
                not self.check_user_for_exist(user['profile_unique_id']):
            self.increment_stat('skipped_profile_duplicate')
            self.log('User with {!r} already exist.'.format(
                user['profile_unique_id']), INFO)
            return

        first_name = xp('First Name', './/span/text()')
        last_name = xp('Last Name', './/span/text()')
        user['user_name'] = '{} {}'.format(first_name, last_name)

        user['user_gender'] = xp('Gender', './/text()')
        user['user_dob'] = xp('DOB', './/text()')

        country = xp('Country', './/span/text()')
        city = xp('City', './/span/text()')
        user['user_location'] = '{}, {}'.format(country, city)

        user['photos_count'] = response.meta['photo_count']

        yield Request(
            '{}/{}'.format(self.BASE_URL, response.meta['photo_album_link']),
            self.parse_photo_albums,
            meta={
                'profile_unique_id': user['profile_unique_id']
            }
        )

        yield user

    def parse_photo_albums(self, response):
        links = response.xpath('//a[strong[contains(text(), "Photos")]]/@href')
        for link in links.extract():
            yield Request(
                '{}/{}'.format(self.BASE_URL, link),
                self.parse_photos,
                meta={
                    'profile_unique_id': response.meta['profile_unique_id']
                }
            )

        # Next photo page
        next_page = self.extract(
            response.xpath('//a[text()="Next"]/@href'))
        if not next_page:
            return

        yield Request(
            '{}/{}'.format(self.BASE_URL, next_page),
            self.parse_photo_albums,
            meta={
                'profile_unique_id': response.meta['profile_unique_id']
            }
        )

    def parse_photos(self, response):
        images = response.xpath('//p[@class="thumb"]/a/img/@src')
        for image in images.extract():
            image_link = image.replace('album/medium', 'album/big')

            photo = \
                self.set_item_default_field(response, self.get_photo_item())
            photo['img_url'] = '{}/{}'.format(self.BASE_URL, image_link)
            photo['profile_unique_id'] = response.meta['profile_unique_id']

            yield photo
